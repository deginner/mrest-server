import json
import pika
from mrest_core.helpers import get_cfg

class MQClient():
    def __init__(self, cfg):
        self.client = pika.BlockingConnection(pika.URLParameters(get_cfg(cfg, 'BROKER_URL')))
        self.pikaChannel = self.client.channel()
        self.pikaChannel.exchange_declare(**get_cfg(cfg, 'EXCHANGE'))
        self.config = cfg

    def publish(self, data):
        self.pikaChannel.basic_publish(body=json.dumps(data),
                                       exchange=get_cfg(self.config, 'EXCHANGE')['exchange'],
                                       routing_key='')

class MQServerMixin(object):
    def __init__(self, config):
        # Configure pika for message forwarding
        pikacfg = get_cfg(config, 'PIKA_CONFIG', False)
        if pikacfg:
            self.mqclient = mqpublisher.MQClient(pikacfg)

