"""
Optional tools for logging in your mrest application.
"""
import os
import logging
import warnings
from graypy import GELFHandler
from mrest_core.helpers import get_cfg
from mrest_server.models import ServerMixin


try:
    from cloghandler import ConcurrentRotatingFileHandler as RotatingFileHandler
except ImportError:
    warnings.warn(
        "cloghandler not available, will use NON-concurrent log handler")
    from logging.handlers import RotatingFileHandler as OrigRotatingFileHandler

    def RotatingFileHandler(*args, **kwargs):
        """
        Function for conforming with ConcurrentRotatingFileHandler
        which accepts a debug parameter.
        """
        kwargs.pop('debug', None)
        return OrigRotatingFileHandler(*args, **kwargs)


def setup_log_handlers(config, fname, formatter=None, **kwargs):
    """
    Create a RotatingFileHandler to be used by a logger, and possibly a
    GELFHandler.

    By default the RotatingFileHandler stores 100 MB before starting a new
    log file and the last 10 log files are kept. The default formatter shows
    the logging level, current time, the function that created the log entry,
    and the specified message.

    :param dict config: The configuration object or dict to use.
    :param str fname: path to the filename where logs will be written to
    :param logging.Formatter formatter: a custom formatter for this logger
    :param kwargs: custom parameters for the RotatingFileHandler
    :rtype: tuple
    """
    if formatter is None:
        formatter = logging.Formatter(
            '%(levelname)s [%(asctime)s] %(funcName)s: %(message)s')

    opts = {'maxBytes': 100 * 1024 * 1024, 'backupCount': 10, 'debug': False}
    opts.update(kwargs)
    handler = RotatingFileHandler(
        os.path.join(
            get_cfg(
                config,
                'LOG_DIR'),
            fname),
        **opts)
    handler.setFormatter(formatter)

    handlers = (handler, )
    use_gelf = get_cfg(config, 'USE_GELF', False)
    if use_gelf:
        gelf_handler = GELFHandler(**use_gelf)
        gelf_handler.setLevel(logging.INFO)  # Ignore DEBUG messages.
        handlers += (gelf_handler, )

    return handlers


class LoggingServerMixin(ServerMixin):
    """
    A Mixin class for a server implementing logging.
    Loads a rotating log file handler from helpers.py
    """

    def __init__(self, config):
        super(LoggingServerMixin, self).__init__(config)
        self.logger = logging.getLogger(self._display_name)
        self.logger.setLevel(logging.INFO)
        log_dir = get_cfg(config, 'LOG_DIR', './')
        log_name = get_cfg(config, 'LOG_NAME', 'MREST-app.log')
        use_gelf = get_cfg(config, 'USE_GELF', False)

        for h in setup_log_handlers(config, log_name):
            self.logger.addHandler(h)
