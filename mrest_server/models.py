"""
Tools for managing models and schemas.
"""
import jsonschema
from mrest_core.error import RouteNotFound
from mrest_core.helpers import get_cfg
from mrest_core.models import BaseModel
from mrest_core.schemas import BaseMixin


class ServerMixin(BaseMixin):
    """
    A Mixin class for use in an mrest client or server application.

    Loads and manages schemas and keyring.
    """

    def __init__(self, config):
        super(ServerMixin, self).__init__(config)
        self._display_name = get_cfg(
            config, 'DISPLAY_NAME', "MREST Application")

    def get_server_info(self):
        """
        Get info about the server itself.
        What authentication requirements does it have? Which ECC curve should
        the client use? What schemas are available?
        """
        return {'name': self._display_name,
                'user_model': 'user',
                'keyring': self.keyring,
                'schemas': self.display_schemas}


class ServerModel(BaseModel):
    """
    An mrest model for a server. Has route handlers pre-defined with "Not found"
    bodies.
    """

    def __init__(self, display_name, name, schema=None):
        super(ServerModel, self).__init__(display_name, name, schema)

    def get(self):
        """
        Handler for GET /<model>
        """
        raise RouteNotFound()

    def post(self):
        """
        Handler for POST /<model>
        """
        raise RouteNotFound()

    def put(self):
        """
        Handler for PUT /<model>
        """
        raise RouteNotFound()

    def delete(self):
        """
        Handler for DELETE /<model>
        """
        raise RouteNotFound()

    def get_one(self, itemid):
        """
        Handler for GET /<model>/<id>
        """
        raise RouteNotFound()

    def post_one(self, itemid):
        """
        Handler for POST /<model>/<id>
        """
        raise RouteNotFound()

    def put_one(self, itemid):
        """
        Handler for PUT /<model>/<id>
        """
        raise RouteNotFound()

    def delete_one(self, itemid):
        """
        Handler for DELETE /<model>/<id>
        """
        raise RouteNotFound()
