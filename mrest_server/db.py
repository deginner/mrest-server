import sqlalchemy as sa
import sqlalchemy.orm as orm
from sqlalchemy.ext.declarative import declarative_base
from alchemyjsonschema import BaseModelWalker, SingleModelWalker, SchemaFactory
from mrest_core.helpers import get_cfg
from mrest_core.schemas import BaseMixin, USER_SCHEMA
from mrest_server.models import ServerModel, ServerMixin

SABase = declarative_base()


def dictify_item(item, model):
    columns = [c.name for c in model.__table__.columns]
    columnitems = dict([(c, getattr(item, c)) for c in columns])
    return columnitems


def query_to_item(query, model):
    if isinstance(query, SABase):
        return dictify_item(query, model)
    else:
        items = []
        for item in query:
            items.append(dictify_item(item, model))
        return items


class SAMixin(ServerMixin):
    """
    A Mixin class for a server implementing SQLAlchemy.
    Loads and manages models, schemas, and keyring.
    Creates and handles SQLAlchemy sessions.
    """

    def __init__(self, config):
        self.models = get_cfg(config, 'MODELS', {})
        super(SAMixin, self).__init__(config)

        sa_engine_url = get_cfg(
            config,
            'SA_ENGINE_URL',
            default='sqlite:///:memory:')
        self.sa = {'engine': sa.create_engine(sa_engine_url)}
        self.__init_db()

    def __init_db(self):
        """
        Initialize the SQLAlchemy session, creating tables as needed.
        """
        # TODO close/clean up existing session
        for mod in self.models:
            # TODO do we always want to call create_all?
            self.models[mod].sa_model.metadata.create_all(self.sa['engine'])
        self.sa['session'] = orm.sessionmaker(bind=self.sa['engine'])()

    @property
    def schemas(self):
        """
        The server's models as json schemas for publishing.
        """
        if len(self._schemas) != len(self.models) + 1:
            for mod in self.models:
                self.add_schema(mod, self.models[mod].schema)
        return self._schemas


class SAModel(ServerModel):

    def __init__(
            self,
            display_name,
            name,
            sa_model,
            excludes=None,
            walker=None,
            schema=None):
        """
        :param str display_name: The display name of the model (typically capitalized)
        :param str name: The model name (lower case, for routing, tables, etc)
        :param SABase sa_model: The SQLAlchemy model
        :param list excludes: a list of excludes to pass to the walker
        :param BaseModelWalker walker: A ModelWalker from alchemyjsonschema
        :param schema: A json schema describing the model (optional so it can be generated)
        """
        super(SAModel, self).__init__(display_name, name, schema=schema)
        if not excludes:
            excludes = []
        self.excludes = excludes
        self.sa_model = sa_model
        if isinstance(walker, BaseModelWalker):
            self.walker = walker
        else:
            self.walker = SingleModelWalker


class UserSA(SABase):
    """model for an api user or item user"""
    __tablename__ = "user"
    # Uncomment if you are extending an earlier experimental build of mrest
    # __table_args__ = {'extend_existing':True}

    id = sa.Column(sa.Integer, primary_key=True, doc="primary key")
    pubhash = sa.Column(sa.String(36), unique=True, nullable=False)
    username = sa.Column(sa.String(36), nullable=False)

    def __repr__(self):
        return "<User(id='%s', username='%s', pubhash='%s')>" % (
            self.id, self.username, self.pubhash)

UserModel = SAModel(
    'User',
    'user',
    UserSA,
    excludes=['id'],
    schema=USER_SCHEMA)
