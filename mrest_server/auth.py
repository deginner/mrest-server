"""
Tools for managing models and schemas.
"""
import jsonschema
from alchemyjsonschema import SingleModelWalker, SchemaFactory
from mrest_core.error import RouteNotFound
from mrest_core.helpers import get_cfg
from mrest_core.models import BaseModel
from mrest_core.schemas import BaseMixin
from mrest_core.auth.mixins import AuthMixin
from mrest_server.db import SAMixin, SAModel


class AuthServerMixin(SAMixin, AuthMixin):
    """
    A Mixin class for use in an mrest client or server application.

    Loads and manages schemas and keyring.
    """

    def __init__(self, config):
        super(AuthServerMixin, self).__init__(config)


class AuthSAModel(SAModel):

    def __init__(
            self,
            display_name,
            name,
            sa_model,
            excludes=None,
            walker=SingleModelWalker,
            schema=None):
        """
        :param str display_name: The display name of the model (typically capitalized)
        :param str name: The model name (lower case, for routing, tables, etc)
        :param SABase sa_model: The SQLAlchemy model
        :param list excludes: a list of excludes to pass to the walker
        :param BaseModelWalker walker: A ModelWalker from alchemyjsonschema
        :param schema: A json schema describing the model (optional so it can be generated)
        """
        SAModel.__init__(
            self,
            display_name,
            name,
            sa_model,
            excludes=excludes,
            walker=walker,
            schema=schema)
#        super(SAModel, self).__init__(display_name, name, sa_model, excludes=excludes, walker=walker, schema=schema)

    @property
    def schema(self):
        """
        Override the parent schema and autogenerate from the SQLAlchemy model.

        :return: The json schema for this model
        """
        if self._schema is None:
            factory = SchemaFactory(self.walker)
            self._schema = factory.__call__(
                self.sa_model, excludes=self.excludes)
            # TODO change to custom route with valid json-reference as per
            # http://tools.ietf.org/html/draft-zyp-json-schema-04#section-6.2
            self._schema['$schema'] = "http://json-schema.org/draft-04/schema#"
            self._schema['routes'] = self.routes
        return self._schema
