from distutils.core import setup

setup(
    name='mrest server',
    version='0.0.1',
    packages=['mrest_server'],
    url='https://bitbucket.org/deginner/mrest-server',
    license='MIT',
    author='deginner',
    author_email='support@deginner.com',
    description='Tools for mrest servers.'
)
