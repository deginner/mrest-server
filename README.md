# mrest server
A library for building mrest servers. This library and the mrest_core library provide all of the tools needed to build an mrest server for the protocol of your choice.


## Deprecation Warning
__This package has been deprecated in favor of [flask-bitjws](https://github.com/deginner/flask-bitjws). For detailed explanation, [read this](https://bitbucket.org/deginner/flask-mrest/wiki/mrest%20deprecation%20plan).__


## Overview
Mrest is an application framework for use with heavily structured (“modeled”) data. Perfect for public APIs and trustless applications.

## Installation
As of right now, installing from the source code is the only method.

`sudo python setup.py install`

## Getting Started
This core library provides tools and mixins for managing your database with SQLAlchemy, advanced authentication, and logging. The best single place to start is by extending one of the mixin classes. This will give your application class the desired mrest server functionality.

```
class MyMrestServerApp(ServerMixin):
    def __init__(self, config):
        super(MyMrestServerApp, self).__init__(config)
```

For example if you extend the ServerMixin class, you will be able to automatically generate the server info by calling `get_server_info`.

To access the database functionality, extend the SAMixin class instead. This will allow you to drive your server with SQLAlchemy models. These models will define what database tables are available, what routes your server should respond to, and, of course, the expected format of messages.

## Configuration
This library provides a number of application seed classes called "mixins", which all expect a single configuration item to be passed to their __init__ method. This item can be a dict, an object or a module, and it will be read in.

### SQLAlchemy
If you are using SAMixin, you can configure your application to use a specific database by setting the `SA_ENGINE_URL` configuration value.

```
SA_ENGINE_URL = 'sqlite:///:memory:'
```

#### Models
It is best to pass your models in as configuration at initialization time.

```
cfg.MODELS = {'user': UserModel()}
app = Application(cfg)
```

The expected format for these models is an instance of `mrest_core.models.BaseModel`. SAServerModel takes your SQLAlchemy model as an input, and will automatically generated json schemas for its models.

### Logging
Optionally configure a log file by using the LoggingServerMixin. This log file will rotate by default, currently has no cleanup.

```
LOG_DIR = "./"
LOG_NAME = 'MREST-app.log'
```

### Bitcoin signing keys
This MREST library uses Bitcoin to authenticate messages. To use it, you will need to generate Bitcoin keys and store them securely.

To generate keys in Python, run the following:

```
import os
from bitcoin.wallet import CKey, P2PKHBitcoinAddress

PRIV_KEY = CKey(os.urandom(64))
PUB_KEY = str(P2PKHBitcoinAddress.from_pubkey(PRIV_KEY.pub))
```

Simply store the private key somewhere safe, and pass it as 'privkey' in the configuration object for AuthMixin. For examples, these are the keys used in the test config file:

```
PRIV_KEY = "L4vB5fomsK8L95wQ7GFzvErYGht49JsCPJyJMHpB4xGM6xgi2jvG"
PUB_KEY = "1F26pNMrywyZJdr22jErtKcjF8R3Ttt55G"
```