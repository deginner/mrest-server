import os
import copy
import hashlib
import json
import unittest
import time
from bitcoin.wallet import *
from bitcoin.signmessage import BitcoinMessage, VerifyMessage, SignMessage
from jsonschema import ValidationError
from mrest_core.auth.message import sign_message, verify_message, encode_signed_message, prepare_mrest_message, create_sign_str, encode_compact_signed_message

import cfg

SERVER_PUB_KEY = "1F26pNMrywyZJdr22jErtKcjF8R3Ttt55G"

newcoin = {'metal': 'UB', 'mint': 'Mars global'}


class Authentication(unittest.TestCase):

    def setUp(self):
        self.privkey = CKey(os.urandom(64))
        self.pubhash = str(P2PKHBitcoinAddress.from_pubkey(self.privkey.pub))
        self.keyring = [self.pubhash]
        self.adata = encode_signed_message(json.dumps(newcoin))
        self.auth_time = str(time.time())
        self.headers = {
            'x-mrest-pubhash-0': self.keyring[0],
            'x-mrest-time-0': self.auth_time,
            'x-mrest-sign-0': sign_message(
                data=self.adata['data'],
                method="RESPONSE",
                auth_time=self.auth_time,
                privkey=self.privkey)}

    def test_sign(self):
        try:
            signatures = verify_message(
                self.adata['data'], self.headers, self.keyring, [
                    self.pubhash], "RESPONSE")
        except Exception as e:
            self.fail("received unexpected Exception: %s" % e)
        self.assertEqual(len(signatures), 1)
        self.assertIn(self.keyring[0], signatures)

    def test_sign_bad_pubhash(self):
        # this exercises the code block beginning at line 126 of
        # mrest_auth.client
        privkey = CKey(os.urandom(64))
        pubhash = str(P2PKHBitcoinAddress.from_pubkey(privkey.pub))
        keyring = [pubhash]
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            self.headers,
            keyring,
            keyring,
            "RESPONSE")

    def test_sign_bad_signature(self):
        privkey = CKey(os.urandom(64))
        headers = copy.copy(self.headers)
        headers['x-mrest-sign-0'] = sign_message(
            data=self.adata['data'],
            method="RESPONSE",
            auth_time=self.auth_time,
            privkey=privkey)
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            headers,
            self.keyring,
            self.keyring,
            "RESPONSE")

    def test_sign_bad_method(self):
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            self.headers,
            self.keyring,
            self.keyring,
            "PUT")

    def test_sign_bad_time(self):
        headers = copy.copy(self.headers)
        headers['x-mrest-time-0'] = str(time.time() + 1)
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            headers,
            self.keyring,
            self.keyring,
            "RESPONSE")

    def test_sign_empty_string(self):
        headers, data = prepare_mrest_message(
            'GET', data="", pubhash=self.pubhash, privkey=self.privkey, headers=None, permissions=['authenticate'])
        packedmess = encode_compact_signed_message(
            'GET', data, headers, "coin")
        try:
            signatures = verify_message(
                data['data'], headers, [
                    self.pubhash], [
                    self.pubhash], "GET")
        except Exception as e:
            print e
            self.fail("received unexpected Exception: %s" % e)
        self.assertEqual(len(signatures), 1)
        self.assertIn(self.pubhash, signatures)


class MultipleSigners(unittest.TestCase):

    def setUp(self):
        self.adata = encode_signed_message(json.dumps(newcoin))

        self.privkey = CKey(os.urandom(64))
        self.pubhash = str(P2PKHBitcoinAddress.from_pubkey(self.privkey.pub))
        self.privkey2 = CKey(os.urandom(64))
        self.pubhash2 = str(P2PKHBitcoinAddress.from_pubkey(self.privkey2.pub))
        self.privkey3 = CKey(os.urandom(64))
        self.pubhash3 = str(P2PKHBitcoinAddress.from_pubkey(self.privkey3.pub))
        self.keyring = [self.pubhash, self.pubhash2]
        self.signers = [self.pubhash, self.pubhash2]
        self.auth_time = str(time.time())
        self.auth_time_2 = str(time.time() + 1)
        self.headers = {'x-mrest-pubhash-0': self.pubhash,
                        'x-mrest-time-0': self.auth_time,
                        'x-mrest-sign-0': sign_message(data=self.adata['data'],
                                                       method="RESPONSE",
                                                       auth_time=self.auth_time,
                                                       privkey=self.privkey),
                        'x-mrest-pubhash-1': self.pubhash2,
                        'x-mrest-time-1': self.auth_time_2,
                        'x-mrest-sign-1': sign_message(data=self.adata['data'],
                                                       method="RESPONSE",
                                                       auth_time=self.auth_time_2,
                                                       privkey=self.privkey2)}

    def test_multiple_good_signers(self):
        signatures = verify_message(
            self.adata['data'],
            self.headers,
            self.keyring,
            self.signers,
            "RESPONSE")
        self.assertEqual(len(signatures), 2)
        self.assertIn(self.pubhash, signatures)
        self.assertIn(self.pubhash2, signatures)

    def test_multiple_one_missing_signer(self):
        # we're expecting 2 signers, but only one signature is received.
        # this exercises the code block beginning at line 148 of
        # mrest_client.auth
        headers = {'x-mrest-pubhash-0': self.pubhash,
                   'x-mrest-time-0': self.auth_time,
                   'x-mrest-sign-0': sign_message(data=self.adata['data'],
                                                  method="RESPONSE",
                                                  auth_time=self.auth_time,
                                                  privkey=self.privkey)}
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            headers,
            self.keyring,
            self.signers,
            "RESPONSE")

    def test_multiple_untrusted_signer(self):
        keyring = [self.pubhash]
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            self.headers,
            keyring,
            self.signers,
            "RESPONSE")

    def test_multiple_bad_sign(self):
        # the wrong key is used in the second signature. should be self.privkey2
        # this exercises the code block beginning at line 142 of
        # mrest_client.auth
        headers = {'x-mrest-pubhash-0': self.pubhash,
                   'x-mrest-time-0': self.auth_time,
                   'x-mrest-sign-0': sign_message(data=self.adata['data'],
                                                  method="RESPONSE",
                                                  auth_time=self.auth_time,
                                                  privkey=self.privkey),
                   'x-mrest-pubhash-1': self.pubhash2,
                   'x-mrest-time-1': self.auth_time_2,
                   'x-mrest-sign-1': sign_message(data=self.adata['data'],
                                                  method="RESPONSE",
                                                  auth_time=self.auth_time_2,
                                                  privkey=self.privkey)}
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            headers,
            self.keyring,
            self.signers,
            "RESPONSE")

    def test_multiple_bad_sign_2(self):
        # the wrong key is used in the first signature. should be self.privkey
        # this exercises the code block beginning at line 128 of
        # mrest_client.auth
        headers = {'x-mrest-pubhash-0': self.pubhash,
                   'x-mrest-time-0': self.auth_time,
                   'x-mrest-sign-0': sign_message(data=self.adata['data'],
                                                  method="RESPONSE",
                                                  auth_time=self.auth_time,
                                                  privkey=self.privkey2),
                   'x-mrest-pubhash-1': self.pubhash2,
                   'x-mrest-time-1': self.auth_time_2,
                   'x-mrest-sign-1': sign_message(data=self.adata['data'],
                                                  method="RESPONSE",
                                                  auth_time=self.auth_time_2,
                                                  privkey=self.privkey2)}
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            headers,
            self.keyring,
            self.signers,
            "RESPONSE")

    def test_multiple_sign_bad_pubhash_2(self):
        # self.pubhash is used in 'x-mrest-pubhash-1' instead of self.pubhash2
        # this exercises the code block beginning at line 134 of
        # mrest-client.auth
        headers = {'x-mrest-pubhash-0': self.pubhash,
                   'x-mrest-time-0': self.auth_time,
                   'x-mrest-sign-0': sign_message(data=self.adata['data'],
                                                  method="RESPONSE",
                                                  auth_time=self.auth_time,
                                                  privkey=self.privkey2),
                   'x-mrest-pubhash-1': self.pubhash,
                   'x-mrest-time-1': self.auth_time_2,
                   'x-mrest-sign-1': sign_message(data=self.adata['data'],
                                                  method="RESPONSE",
                                                  auth_time=self.auth_time_2,
                                                  privkey=self.privkey2)}
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            headers,
            self.keyring,
            self.signers,
            "RESPONSE")

    def test_multiple_sign_bad_time_2(self):
        # str(time.time() +1) is expected value for 'x-mrest-time-1'
        # this exercises the code block beginning at line 143 of
        # mrest_client.auth
        headers = copy.copy(self.headers)
        headers['x-mrest-time-1'] = str(time.time())
        self.assertRaises(
            Exception,
            verify_message,
            self.adata['data'],
            headers,
            self.keyring,
            self.keyring,
            "RESPONSE")


if __name__ == '__main__':
    test_suite = []
    for cls in (Authentication,
                MultipleSigners):
        test_suite.append(unittest.TestLoader().loadTestsFromTestCase(cls))

    for suite in test_suite:
        result = unittest.TextTestRunner(verbosity=2).run(suite)
