import json
import urllib
from mrest_core.schemas import USER_SCHEMA

# MREST app configuration
DISPLAY_NAME = 'Coin collection'
SA_ENGINE_URL = 'sqlite:///:memory:'

# Add authentication details (bitcoin key pair). Values will be ignored if
# no auth routes are specified.
PRIV_KEY = "L4vB5fomsK8L95wQ7GFzvErYGht49JsCPJyJMHpB4xGM6xgi2jvG"
PUB_KEY = "1F26pNMrywyZJdr22jErtKcjF8R3Ttt55G"

# if True, all responses will be signed using the key above
SIGN_RESPONSES = True
CORS = True

# Pika configuration, for SockJS MQ
PIKA_CONFIG = {
    'EXCHANGE': {'exchange': 'sockjsmq', 'exchange_type': 'fanout'},
    'url': "amqp://guest:guest@127.0.0.1:5672/%2F",  # %%2F is "/" encoded
}

PIKA_CONFIG['params'] = {
    'connection_attempts': 3,
    'heartbeat_interval': 3600
}

PIKA_CONFIG['BROKER_URL'] = "%s?%s" % (
    PIKA_CONFIG['url'], urllib.urlencode(PIKA_CONFIG['params']))

# optionally add models and schemas as configuration parameters
MODELS = {}
SCHEMAS = {'user': USER_SCHEMA}

# Logging configuration
LOG_DIR = "./"
LOG_NAME = 'MREST-app.log'
USE_GELF = False
