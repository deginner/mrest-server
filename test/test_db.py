import json
import copy
import unittest
from mrest_server.db import SAMixin, UserSA, UserModel
import cfg


class Initialization(unittest.TestCase):

    def test_sa_server_mixin(self):
        cfg.MODELS = {'user': UserModel}
        sas_mixin = SAMixin(cfg)
        self.assertEqual(len(sas_mixin.models), 1)
        self.assertEqual(len(sas_mixin.schemas), 2)

    def test_sa_server_generate_schemas(self):
        copy_SCHEMAS = copy.deepcopy(cfg.SCHEMAS)
        cfg.SCHEMAS = {}
        cfg.MODELS = {'user': UserModel}
        sas_mixin = SAMixin(cfg)
        self.assertEqual(len(sas_mixin.models), 1)
        self.assertEqual(len(sas_mixin.schemas), 2)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Initialization)
    result = unittest.TextTestRunner(verbosity=2).run(suite)
